package com.speakatalka.localization.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(schema = "main", name = "field_value")
public class FieldValue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "field_value_id")
    @JsonIgnore
    private Integer fieldValueId;

    @Column(name = "field_id")
    private Integer fieldId;

    @OneToOne(fetch = FetchType.LAZY)// "field_lang")
    private LocalLang fieldLang;

    @Column(name = "field_value")
    private Integer fieldValue;
}
