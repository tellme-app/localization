package com.speakatalka.localization.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(schema = "main", name = "local_lang")
public class LocalLang {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "local_id")
    @JsonIgnore
    private Integer localId;

    @Column(name = "local_name")
    private String localName;

    @Column(name = "local_real_name")
    private String realName;
}
