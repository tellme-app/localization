package com.speakatalka.localization.service;

import com.speakatalka.localization.entity.Fields;
import com.speakatalka.localization.repository.FieldsRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.AllArgsConstructor;

import javax.inject.Singleton;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@Singleton
public class FieldsService {

    private final FieldsRepository fieldsRepository;

    public Map<String, String> getFields(String dest, String localName){
        var result = fieldsRepository.getAllByDestinationAndLocalName(dest, localName)
                .stream()
                .collect(Collectors.toMap(Fields::getFieldName, Fields::getFieldValue));
        if (result.isEmpty())
            throw new HttpStatusException(HttpStatus.NOT_FOUND, "Данные не найдены");
        return result;
    }
}
