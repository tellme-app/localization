package com.speakatalka.localization.controller;

import com.speakatalka.localization.entity.LocalLang;
import com.speakatalka.localization.repository.LocalLangRepository;
import com.speakatalka.localization.service.FieldsService;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Controller
@RequiredArgsConstructor
public class MainController {

    private final FieldsService fieldsService;
    private final LocalLangRepository localLangRepository;

    @Get
    public Map<String, String> getLocalization(@QueryValue String dest, @QueryValue String localName){
        return fieldsService.getFields(dest, localName);
    }

    @Get("/available")
    public Iterable<LocalLang> getAvailableLanguages() {
        return localLangRepository.findAll();
    }
}
