package com.speakatalka.localization.repository;

import com.speakatalka.localization.entity.Fields;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;

@Repository
public interface FieldsRepository extends CrudRepository<Fields, Integer> {

    @Query(value = "select fields.field_id, field_name, field_value.field_value " +
            "from main.fields" +
            "         left join main.field_to_destination on fields.field_id = field_to_destination.field_id" +
            "         left join main.field_destination" +
            "                   on field_to_destination.field_destination_id = field_destination.field_destination_id" +
            "         left join main.field_value on fields.field_id = field_value.field_id" +
            "         left join main.local_lang on field_value.field_lang = local_lang.local_id" +
            " where field_destination.field_destination_name=:destination and local_name=:localName", nativeQuery = true)
    List<Fields> getAllByDestinationAndLocalName(String destination, String localName);
}
