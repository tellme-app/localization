package com.speakatalka.localization.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(schema = "main", name = "field_destination")
public class FieldDestination {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "field_destination_id")
    @JsonIgnore
    private Integer fieldDestinationId;

    @Column(name = "field_destination_name")
    private String fieldDestinationName;
}
