package com.speakatalka.localization.repository;

import com.speakatalka.localization.entity.LocalLang;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;

@Repository
public interface LocalLangRepository extends CrudRepository<LocalLang, Integer> {

//    @Query(value = "select t.local_name from main.local_lang t", nativeQuery = true)
//    List<String> getAllLanguages();

    @Query("select t from LocalLang t")
    List<LocalLang> getAll();
}
